TAG?=latest

default: build
build:
	mvn -DskipTests clean package
run:
	mvn spring-boot:run
image: clean build
	docker build --no-cache -t s3.ok.un-net.com:5000/oneclick/oss:$(TAG) .
push: image
	docker push s3.ok.un-net.com:5000/oneclick/oss:$(TAG)
rmi:
	docker  images|grep none|awk '{print $3}'|xargs docker rmi
clean:
	rm -rf ./target/

# release
scdx:
	./hack/build_scdx.sh
lndx:
	./hack/build_lndx.sh

# dev
preview: push
#注意权限问题
	chmod  a+x  ./hack/*.sh
	./hack/update_local.sh
