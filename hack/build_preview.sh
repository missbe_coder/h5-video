#!/bin/bash

cd `dirname $0`
cd ..

echo "spring.profiles.active=preview" > src/main/resources/application.properties

mvn -DskipTests clean package

echo "spring.profiles.active=dev" > src/main/resources/application.properties

echo "Build release package for preview success"