package com.unnet.yjs.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.unnet.yjs.entity.Rescource;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:55
 *
 *
 **/
public interface RescourceDao extends BaseMapper<Rescource> {

}
