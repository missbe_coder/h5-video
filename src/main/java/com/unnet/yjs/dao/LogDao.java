package com.unnet.yjs.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.unnet.yjs.entity.Log;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 * 系统日志 Mapper 接口
 * </p>
 *
 * @author wangl
 * @since 2018-01-14
 */
@Repository
public interface LogDao extends BaseMapper<Log> {

    List<Map> selectSelfMonthData();
}
