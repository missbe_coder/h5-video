package com.unnet.yjs.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c) 2018. missbe
 *
 * @author lyg  19-5-21 下午9:43
 **/
@RestController
public class BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseController.class);
}
