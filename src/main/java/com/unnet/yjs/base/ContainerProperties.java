package com.unnet.yjs.base;

import com.xiaoleilu.hutool.util.StrUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 *
 * @author lyg   19-7-24 下午3:38
 **/


@Setter
@Getter
@ToString
@Component("containerProperties")
public class ContainerProperties {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainerProperties.class);
    /**
     * 文件上传类型
     */
    @Value("${yjs.file.upload.type}")
    private String fileUploadType;

    /**
     * 对象存储端点
     */
    @Value("${yjs.file.upload.oss.endpoint}")
    private String ossEndPoint;

    /**
     * 对象存储访问密钥
     */
    @Value("${yjs.file.upload.oss.access.key}")
    private String ossAccessKey;

    /**
     * 对象存储秘钥
     */
    @Value("${yjs.file.upload.oss.secret.key}")
    private String ossSecretKey;

    /**
     * 对象存储桶名称
     */
    @Value("${yjs.file.upload.oss.bucket.name}")
    private String ossBucketName;

    @Value(value = "${spring.profiles.active}")
    private static String profiles;

    @Value("${yjs.cross.domain.key}")
    private String key;
    /**
     * 文件服务器地址和端口地址
     */
    @Value("${yjs.resource.file.context}")
    private String fileServerContext;

    @Value("${yjs.resource.file.cache.location}")
    private String fileCacheLocation;

    @Value("${yjs.resource.file.cache.strategy}")
    private boolean cache;

    @Value("${yjs.resource.file.cache.restricted.size}")
    private String restrictSize;

    public String getRestrictSize() {
        if (StrUtil.isBlank(restrictSize)) {
            return String.valueOf(5);
        }
        return restrictSize;
    }

    public void setRestrictSize(String restrictSize) {
        this.restrictSize = restrictSize;
    }

    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public String getFileCacheLocation() {
        return fileCacheLocation;
    }

    public void setFileCacheLocation(String fileCacheLocation) {
        this.fileCacheLocation = fileCacheLocation;
    }

    public String getFileServerContext() {
        return fileServerContext;
    }

    public void setFileServerContext(String fileServerContext) {
        this.fileServerContext = fileServerContext;
    }

    public String getFileUploadType() {
        return fileUploadType;
    }

    public void setFileUploadType(String fileUploadType) {
        this.fileUploadType = fileUploadType;
    }

    public String getOssEndPoint() {
        return ossEndPoint;
    }

    public void setOssEndPoint(String ossEndPoint) {
        this.ossEndPoint = ossEndPoint;
    }

    public String getOssAccessKey() {
        return ossAccessKey;
    }

    public void setOssAccessKey(String ossAccessKey) {
        this.ossAccessKey = ossAccessKey;
    }

    public String getOssSecretKey() {
        return ossSecretKey;
    }

    public void setOssSecretKey(String ossSecretKey) {
        this.ossSecretKey = ossSecretKey;
    }

    public String getOssBucketName() {
        return ossBucketName;
    }

    public void setOssBucketName(String ossBucketName) {
        this.ossBucketName = ossBucketName;
    }

    public static String getProfiles() {
        return profiles;
    }

    public static void setProfiles(String profiles) {
        ContainerProperties.profiles = profiles;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
