package com.unnet.yjs.controller.api;

import com.unnet.yjs.annotation.HttpMethod;
import com.unnet.yjs.exception.MyException;
import com.xiaoleilu.hutool.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/
@Controller
@Api(tags = {"RootController"},description = "首页控制器")
public class RootController implements ErrorController {
    private static final String ERROR_PATH = "/error";
    private static final Logger LOGGER = LoggerFactory.getLogger(RootController.class);

    @RequestMapping("/")
    @ApiOperation(value = "首页提示", httpMethod = HttpMethod.GET)
    public String index() {
        return "video/video";
    }

    @ApiOperation(value = "错误提示", httpMethod = HttpMethod.GET)
    @RequestMapping(value = ERROR_PATH, method = RequestMethod.GET)
    public String error(HttpServletRequest request) {
        LOGGER.info(request.getRemoteHost() + " invoke  file-upload error.");
        return "yjs service could not handle this request, make sure your request is valid or call service creator for help :-(";
    }


    @ApiOperation(value = "全局异常错误测试", httpMethod = HttpMethod.GET)
    @ApiImplicitParams({
            @ApiImplicitParam(name="code",defaultValue = "404",required = true,value = "错误异常代码[404|500|600]",paramType = "path",dataType = "string")
    })
    @RequestMapping(value = "/global/error/{code}.do", method = RequestMethod.GET)
    public void globalError(@PathVariable("code") String code) {
        LOGGER.info(code + "->全局异常错误测试.");
        if(StrUtil.equals("500",code)){
            throw new MyException("500内部服务器错误测试",500);
        }else if(StrUtil.equals("404",code)){
            throw new MyException("404错误测试",404);
        }else if(StrUtil.equals("600",code)){
            throw new MyException("未授权异常测试");
        }else{
            throw new NullPointerException();
        }
    }

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
