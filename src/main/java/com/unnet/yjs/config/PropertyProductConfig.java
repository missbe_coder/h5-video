package com.unnet.yjs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-10 下午7:48
 *
 *
 **/
@Profile({"prod","test"})
@Configuration
@PropertySource(value = "classpath:conf/config-prod.properties",ignoreResourceNotFound = true,encoding = "UTF-8")
public class PropertyProductConfig {
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
        return new PropertySourcesPlaceholderConfigurer();
    }
}
