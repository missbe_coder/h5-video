package com.unnet.yjs.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-24 下午2:21
 *
 *
 **/

@Configuration
public class MybatisPlusConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(MybatisPlusConfig.class);

    /***
	 * mybatis-plus的性能优化-设置最大执行时长和格式化sql输出
     */
	@Bean
	@Profile({"dev","test"})
	public PerformanceInterceptor performanceInterceptor() {
        LOGGER.info("dev和test环境下加载PerformanceInterceptor性能分析插件.");
		PerformanceInterceptor performanceInterceptor=new PerformanceInterceptor();
		/*<!-- SQL 执行性能分析，开发环境使用，线上不推荐。 maxTime 指的是 sql 最大执行时长 -->*/
		performanceInterceptor.setMaxTime(1000);
		/*<!--SQL是否格式化 默认false-->*/
		performanceInterceptor.setFormat(true);
		return performanceInterceptor;
	}

	/**
	 * mybatis-plus分页插件-设置分页类型为mysql和本地分页
	 */
	@Bean
	public PaginationInterceptor paginationInterceptor() {
        LOGGER.info("加载PaginationInterceptor本地分页插件.");
		PaginationInterceptor page = new PaginationInterceptor();
        page.setLocalPage(true);
		page.setDialectType("mysql");
		return page;
	}
}
