package com.unnet.yjs.config;

import com.unnet.yjs.base.ContainerProperties;
import com.unnet.yjs.util.IOssOperation;
import com.unnet.yjs.util.MinIoOssOperation;
import com.xiaoleilu.hutool.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-29 下午9:57
 *
 *
 **/
@Configuration
public class OssConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(OssConfig.class);
    @Resource
    private ContainerProperties containerProperties;

    @Bean("ossStreamConvert")
    public IOssOperation iOssStreamConvert(){
        String config = containerProperties.getFileUploadType();
        if(StrUtil.equalsIgnoreCase(config,"paas")){
            LOGGER.info("加载PaaS对象存储配置.");
            return minIoOssTool();
        }else if(StrUtil.equalsIgnoreCase(config,"myOss")){
            LOGGER.info("加载MyOSS对象存储配置.");
            return minIoOssTool();
        }
        return null;
    }

    @Bean
    public MinIoOssOperation minIoOssTool(){
        return new MinIoOssOperation();
    }
}
