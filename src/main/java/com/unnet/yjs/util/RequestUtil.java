package com.unnet.yjs.util;

import com.xiaoleilu.hutool.map.MapUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class RequestUtil {
    /**
     *  根据HttpServletRequest请求中前缀获取参数并且保留原来的请缀
     * @param request HttpServletRequest对象
     * @param prefix 前缀串
     * @return 参数map
     */
    public static Map<String,Object> getParametersStartingWithPrefix(HttpServletRequest request,String prefix){
        Map<String,Object> cloneMap = MapUtil.newHashMap();
        ///参数空指针判断
        Map<String, Object> map = WebUtils.getParametersStartingWith(request, prefix);
        for(String key : map.keySet()){
            cloneMap.put(StrUtil.addPrefixIfNot(key,prefix),map.get(key));
        }
        return cloneMap;
    }

}
