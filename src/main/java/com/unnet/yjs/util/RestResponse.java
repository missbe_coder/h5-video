package com.unnet.yjs.util;

import com.xiaoleilu.hutool.json.JSONArray;

import java.util.Collections;
import java.util.HashMap;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/
@SuppressWarnings("serial")
public class RestResponse extends HashMap<String, Object> {
    public static RestResponse success(){
        return success("成功");
    }
    public static RestResponse success(String message){
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(true);
        restResponse.setMessage(message);
        restResponse.setCode(0);
        return restResponse;
    }

    public static RestResponse failure(String message){
        RestResponse restResponse = new RestResponse();
        restResponse.setSuccess(false);
        restResponse.setMessage(message);
        restResponse.setCode(-1);
        return restResponse;
    }
    /**
     *  返回给客户端的成功信息
     * @return 结果JSONArray形式
     * [{"result":"message","code":"0"}]
     */
    public static JSONArray successResult2Client(){
        RestResponse restResponse = new RestResponse();
        restResponse.setMessage("invoke success.");
        restResponse.put("code","0");
        return new JSONArray(Collections.singletonList(restResponse));
    }

    /**
     *  返回给客户端的错误信息
     * @param message 错误信息
     * @return 结果JSONArray形式
     * [{"result":"message","code":"-1"}]
     */
    public static JSONArray failureResult2Client(String message){
        RestResponse restResponse = new RestResponse();
        restResponse.setResult(message);
        restResponse.put("code","-1");

        return new JSONArray(Collections.singletonList(restResponse));
    }
    public RestResponse setCode(Integer code) {
        if (code != null) {
            put("code", code);
        }
        return this;
    }

    public int getCode() {
        Object o = this.get("code");
        return o instanceof Integer ? (Integer)o : (o instanceof String ? Integer.parseInt((String) o) : -1);
    }


    public RestResponse setSuccess(Boolean success) {
        if (success != null) {
            put("success", success);
        }
        return this;
    }

    public RestResponse setResult(String message) {
        if (message != null) {
            put("result", message);
        }
        return this;
    }

    public String getResult() {
        Object o = this.get("result");
        return o instanceof String ? (String) o : o.toString();
    }



    public RestResponse setMessage(String message) {
        if (message != null) {
            put("message", message);
        }
        return this;
    }

    public RestResponse setData(Object data) {
        if (data != null) {
            put("data", data);
        }
        return this;
    }

    public RestResponse setPage(Integer page) {
        if (page != null) {
            put("page", page);
        }
        return this;
    }
    
    public RestResponse setCurrentPage(Integer currentPage){
    	if (currentPage != null) {
            put("currentPage", currentPage);
        }
        return this;
    }

    public RestResponse setLimit(Integer limit) {
        if (limit != null) {
            put("limit", limit);
        }
        return this;
    }

    public RestResponse setTotal(Long total) {
        if (total != null) {
            put("total", total);
        }
        return this;
    }

    public RestResponse setAny(String key, Object value) {
        if (key != null && value != null) {
            put(key, value);
        }
        return this;
    }
}
