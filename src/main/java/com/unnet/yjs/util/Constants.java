package com.unnet.yjs.util;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/

public class Constants {
	/**
	 * shiro采用加密算法
	 */
	public static final String HASH_ALGORITHM = "SHA-1";
	/**
	 * 生成Hash值的迭代次数 
	 */
	public static final int HASH_INTERATIONS = 1024;
	/**
	 * 生成盐的长度
	 */
	public static final int SALT_SIZE = 8;

	/**
	 * 验证码
	 */
	public static final String VALIDATE_CODE = "validateCode";

	/**
	 *系统用户默认密码
	 */
	public static final String DEFAULT_PASSWORD = "123456";

	/**
	 * 定时任务状态:正常
	 */
	public static final Integer QUARTZ_STATUS_NOMAL = 0;
	/**
	 * 定时任务状态:暂停
	 */
	public static final Integer QUARTZ_STATUS_PUSH = 1;

	/**
	 * 评论类型：1文章评论
	 */
	public static final Integer COMMENT_TYPE_ARTICLE_COMMENT = 1;
	/**
	 * 评论类型：2.系统留言
	 */
	public static final Integer COMMENT_TYPE_LEVING_A_MESSAGE = 2;
	/**API Groups 版本前缀*/
	public static final String V1_API_PREFIX = "/api/yjs/v1/index/";
	/**API Client Gatewat 版本前缀*/
	public static final String CLIENT_V1_API_PREFIX = "/api/yjs/v1/client/";
	/**API mgmt Gatewat 版本前缀*/
	public static final String MGMT_V1_API_PREFIX = "/api/yjs/v1/mgmt/";
	/**API调用设置请求头*/
	public static final String USER_AGENTt_DAO = "User-AgentDao";
	/**API调用设置请求头*/
	public static final String QUARTZ_API_GROUPS = "API_GROUPS";
	/**API调用设置请求头*/
	public static final String QUARTZ_CLIENT_GATEWAY = "CLIENT_GATEWAY";

}
