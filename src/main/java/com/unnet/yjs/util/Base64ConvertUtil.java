package com.unnet.yjs.util;

import com.unnet.yjs.entity.Base64MultipartFile;
import com.unnet.yjs.exception.MyException;
import com.xiaoleilu.hutool.codec.Base64;
import com.xiaoleilu.hutool.date.DateUtil;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.util.Date;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-9 下午7:22
 *
 *  文件和Base64互转工具
 **/

public class Base64ConvertUtil {

    /**
     * Base64转化为MultipartFile
     *
     * @param data 前台传过来base64的编码
     * @param fileName 图片的文件名
     * @return
     */
    public static MultipartFile base64toMultipart(String data, String fileName) {
        try {
            String[] baseStrs = data.split(",");
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] b = decoder.decodeBuffer(baseStrs[1]);
            for(int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256;
                }
            }
            return new Base64MultipartFile(b, baseStrs[0] , fileName);
        } catch (IOException e) {
            throw new MyException("IO流异常" , e);
        }
    }

    public static File base64ToFile(String base64) {
        String fileName = DateUtil.format(new Date(),"yyyyMMddhhmmss")+System.currentTimeMillis();
        return  base64ToFile(base64,fileName);
    }
    /**
     * base64字符串转文件
     */
    public static File base64ToFile(String base64,String fileName) {
        File file = null;
        FileOutputStream out = null;
        try {
            // 解码，然后将字节转换为文件
            file = new File(fileName);
            if (!file.exists()) {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            }
            // 将字符串转换为byte数组
            byte[] bytes = Base64.decode(base64,"utf-8");
            ByteArrayInputStream in = new ByteArrayInputStream(bytes);
            byte[] buffer = new byte[1024];
            out = new FileOutputStream(file);
            int byteread;
            while ((byteread = in.read(buffer)) != -1) {
                // 文件写操作
                out.write(buffer, 0, byteread);
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (out!= null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
    /**
     * 文件转base64字符串
     */
    public static String fileToBase64(File file) {
        String base64 = null;
        InputStream in = null;
        try {
            in = new FileInputStream(file);
            byte[] bytes = new byte[in.available()];
            int length = in.read(bytes);
            //encodeToString(bytes, 0, length, Base64.DEFAULT);
            base64 = Base64.encode(bytes, "utf-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return base64;
    }
}
