package com.unnet.yjs.util;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-6-2 下午1:43
 *
 * remote api 调用方法
 **/

public class WebserviceUtil {
    private static final Logger logger = LoggerFactory.getLogger(WebserviceUtil.class);

    /**
     * Webservice接口请求方法
     * @param webserviceUrl Webservice接口地址
     * @param parmeter 请求参数
     * @return 执行结果
     * @throws ServiceException 服务获取异常
     * @throws RemoteException 服务执行异常
     */
     public  static  String postIWebserviceData(String webserviceUrl,String invokeMethod, String parmeter) throws ServiceException, RemoteException {
        if(webserviceUrl == null){
            throw new NullPointerException("Webservice请求接口地址不能为NULL...");
        }
        return processWebserviceRequest(webserviceUrl,invokeMethod,parmeter);
    }

    /**
     * Webservice请求核心处理方法
     * @param webserviceUrl Webservice接口地址
     * @param parmeter 请求参数
     * @return 执行结果
     * @throws ServiceException 服务获取异常
     * @throws RemoteException 服务执行异常
     */
    private static String processWebserviceRequest(String webserviceUrl,String invokeMethod,String parmeter)throws ServiceException, RemoteException{
        String sssoapxmlns = "http://manager.service.sc.project.base.com";
        ///转发xml数据请求Webservice接口
        ///创建服务对象
        Service webservice = new Service();
        Call webserviceCall = (Call) webservice.createCall();
        ///设置超时时间4000毫秒
        webserviceCall.setTimeout(4000);
        ///设置访问操作点
        webserviceCall.setTargetEndpointAddress(webserviceUrl);
        ///设置操作名,联调时修改
        webserviceCall.setOperationName(new QName(sssoapxmlns,invokeMethod));
        ///设置返回类
//        webserviceCall.setReturnClass(String.class);
        ///执行接口，获取执行结果数据
        String resultMsg = (String) webserviceCall.invoke(new Object[]{parmeter});
        if(resultMsg == null || resultMsg.isEmpty()){
            resultMsg = "";
            logger.error("请求Webservice接口失败，获取数据为空...");
        }
        return resultMsg;
    }
}
