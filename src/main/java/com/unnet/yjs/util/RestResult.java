package com.unnet.yjs.util;

import java.util.Map;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/

public class RestResult<K,V> {
    private Integer code = 0;
    private Integer count;
    private Map<K,V> data;
    private String msg = "";

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Map<K, V> getData() {
        return data;
    }

    public void setData(Map<K, V> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
