package com.unnet.yjs.util;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/

public class JSONUtil {
    private static final Logger logger = LoggerFactory.getLogger(JSONUtil.class);

    private static String returnJSONResult(boolean success, String state, String message, Object entity){
        Map<String,Object> map = new HashMap<>(4);
        map.put("isSuccess",success);
        map.put("stateCode",state);
        map.put("message",message);
        map.put("entity",entity);
        logger.info("返回JSON字符串：" + JSONObject.toJSONString(map));
        return JSONObject.toJSONString(map);
    }

    /**
     * 设置失败消息JSON字符串
     */
    public static String returnFailReuslt(String message){
        return returnJSONResult(false, "200" ,message,null);
    }

    /**
     * 设置返回实体JSON字符串
     */
    public static String returnEntityReuslt(Object entity){
        return returnJSONResult(true, "200" ,null,entity);
    }

    /**
     * 设置403权限不足JSON字符串
     */
    public static String returnForbiddenResult(){
        return returnJSONResult(false, "403" ,"权限不足!! 禁止访问!!",null);
    }

    /**
     * 返回成功消息JSON字符串
     */
    public static String returnSuccessResult(String message){
        return returnJSONResult(true, "200" ,message,null);
    }
}
