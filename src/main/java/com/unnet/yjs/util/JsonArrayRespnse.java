package com.unnet.yjs.util;

import com.xiaoleilu.hutool.json.JSONArray;
import com.xiaoleilu.hutool.json.JSONObject;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-11 下午2:07
 *
 *
 **/

public class JsonArrayRespnse {
    private JSONObject jsonObject;

    public JsonArrayRespnse(){
        jsonObject = new JSONObject(4);
    }

    public JsonArrayRespnse(int capacity){
        jsonObject = new JSONObject(capacity);
    }


    public JsonArrayRespnse setType(Integer typeValue){
        if(typeValue != null) {
            jsonObject.put("type", typeValue);
        }
        return this;
    }
     public JsonArrayRespnse setkeyValue(String key,Object value){
        if(key != null && value != null){
            jsonObject.put(key, value);
        }
        return  this;
     }

     public JsonArrayRespnse setExpertOfflineFlag(Boolean expertOfflineFlag){
        if(expertOfflineFlag != null){
            jsonObject.put("expertOfflineFlag",expertOfflineFlag);
        }
        return this;
     }

     public JsonArrayRespnse setExpertOnlineFlag(Boolean expertOnlineFlag){
        if(expertOnlineFlag != null){
            jsonObject.put("expertOnlineFlag",expertOnlineFlag);
        }
        return this;
     }

     public JsonArrayRespnse setExpertRemoteTimeFlag(Boolean expertRemoteTimeFlag){
        if(expertRemoteTimeFlag != null){
            jsonObject.put("expertRemoteTimeFlag",expertRemoteTimeFlag);
        }
        return this;
     }

     public JSONArray toJsonArray(){
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(this.jsonObject);
        return jsonArray;
     }

}
