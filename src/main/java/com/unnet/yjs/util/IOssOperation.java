package com.unnet.yjs.util;

import io.minio.ObjectStat;
import io.minio.errors.*;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-29 下午9:29
 *
 *
 **/

public interface IOssOperation {
    /**
     * 上传文件到对象存储库
     *
     * @param stream   源文件流
     * @param fileSize 源文件大小
     * @param filename 源文件名称
     */
    boolean store(InputStream stream, long fileSize, String filename) throws Exception ;

    /**
     * 从对象存储中根据文件名称获取文件流
     *
     * @param filename 文件名
     * @return 文件流
     */
    InputStream load(String filename) throws Exception;

    /**
     * 从对象存储中根据文件名称获取文件流
     * @param filename filename 文件名
     * @param offset 偏移量
     * @param length 长度
     * @return 文件流
     */
    InputStream load(String filename,long offset,long length) throws Exception;

    /**
     * 从对象存储中删除文件对象
     * @param fileNames 文件对象名称列表
     * @return 删除结果
     */
    boolean remove(List<String> fileNames);

    /**
     * 文件长度
     * @param filename 文件名称
     * @return 文件长度
     */
    long length(String filename) throws Exception;
}
