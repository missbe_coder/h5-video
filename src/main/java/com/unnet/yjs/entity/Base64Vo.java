package com.unnet.yjs.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   2019/8/7 下午3:02
 *
 *
 **/
@ToString
@Setter
@Getter
public class Base64Vo {
    private String file;
    private String name;
    private String s_areaId;
    private String s_rename;
    private String s_remarks;
}
