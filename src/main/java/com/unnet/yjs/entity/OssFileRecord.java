package com.unnet.yjs.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.unnet.yjs.base.DataEntity;
import io.swagger.annotations.ApiModelProperty;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:55
 *
 *
 **/

@TableName("oss_file_visit_record")
public class OssFileRecord extends DataEntity<OssFileRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * 文件名称
     */
	@TableField("file_name")
	private String fileName;
    /**
     * 文件大小
     */
    @TableField("file_length")
	private String fileLength;
    /**
     * 文件访问次数
     */
	@TableField("visit_count")
	private int visitCount;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileLength() {
		return fileLength;
	}

	public void setFileLength(String fileLength) {
		this.fileLength = fileLength;
	}

	public int getVisitCount() {
		return visitCount;
	}

	public void setVisitCount(int visitCount) {
		this.visitCount = visitCount;
	}

	@Override
	public String toString() {
		return "OssFileRecord{" + "fileName='" + fileName + '\'' + ", fileLength='" + fileLength + '\'' + ", visitCount='" + visitCount + '\'' + '}';
	}
}
