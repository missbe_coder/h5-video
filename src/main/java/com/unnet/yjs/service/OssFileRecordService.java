package com.unnet.yjs.service;

import com.baomidou.mybatisplus.service.IService;
import com.unnet.yjs.entity.OssFileRecord;
import com.unnet.yjs.entity.Rescource;

import java.util.List;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:56
 *
 *
 **/

public interface OssFileRecordService extends IService<OssFileRecord> {
    /**
     * 根据文件名字查找对象存储文件访问历史对象
     * @param fileName 文件名字
     * @return 对象存储对象
     */
    OssFileRecord findByFileName(String fileName);

    /**
     * 查找所有记录，按照文件访问次数升序和文件大小升序联合排序
     * @return 文件列表
     */
    List<OssFileRecord> findAll();

    void insertDeleteRecord(String fileName);

    List<OssFileRecord> findWaitDeleteRecord();
}
