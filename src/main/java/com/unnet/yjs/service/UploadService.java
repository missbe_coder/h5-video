package com.unnet.yjs.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:54
 *
 *
 **/
public interface UploadService {
    /**
     * 文件上传方法
     * @param file MultipartFile文件对象
     * @return  文件在云上的地址
     */
    String upload(MultipartFile file, Map<String, Object> map) throws IOException, NoSuchAlgorithmException;

    /**
     * 文件上传方法
     * @param file MultipartFile文件对象
     * @return  文件在云上的地址
     */
    String upload(MultipartFile file, String directoryName, Map<String, Object> map) throws IOException, NoSuchAlgorithmException;


    /**
     * 删除已经上传到云上的文件
     * @param fileNames 文件名称列表
     * @return 是否删除成功
     */
    Boolean delete(List<String> fileNames);

    /**
     * 删除已经上传到云上的文件
     * @param path 文件路径
     * @return 是否删除成功
     */
    Boolean delete(String path);

    /**
     * 上传网络文件
     * @param url 网络文件的地址
     * @return  文件在云上的地址
     */
    String uploadNetFile(String url) throws IOException, NoSuchAlgorithmException;

    /**
     * 上传本地指定路径的图片
     * @param localPath  (指的是用户的)本地路径
     */
    String uploadLocalImg(String localPath);

    /**
     * 上传base64格式的文件
     * @param base64 文件的base64编码
     */
    String uploadBase64(String base64);

}
