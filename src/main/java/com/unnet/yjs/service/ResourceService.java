package com.unnet.yjs.service;

import com.baomidou.mybatisplus.service.IService;
import com.unnet.yjs.entity.Rescource;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:56
 *
 *
 **/

public interface ResourceService extends IService<Rescource> {

    int getCountByHash(String hash);

    Rescource getRescourceByHash(String hash);

}
