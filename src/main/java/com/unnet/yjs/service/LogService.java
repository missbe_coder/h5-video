package com.unnet.yjs.service;

import com.baomidou.mybatisplus.service.IService;
import com.unnet.yjs.entity.Log;

import java.util.List;

/**
 *  Email: love1208tt@foxmail.com
 *  Copyright (c) 2018. missbe
 *  @author lyg  19-5-21 下午9:43
 *
 **/

/**
 * <p>
 * 系统日志 服务类
 * </p>
 *
 * @author wangl
 * @since 2018-01-13
 */
public interface LogService extends IService<Log> {

    List<Integer> selectSelfMonthData();

}
