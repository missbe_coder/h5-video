package com.unnet.yjs.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.unnet.yjs.dao.OssFileRecordDao;
import com.unnet.yjs.entity.OssFileRecord;
import com.unnet.yjs.service.OssFileRecordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:56
 *
 *
 **/
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class OssFileRecordServiceImpl extends ServiceImpl<OssFileRecordDao, OssFileRecord> implements OssFileRecordService {


    @Override
    public OssFileRecord findByFileName(String fileName) {
        EntityWrapper<OssFileRecord> wrapper = new EntityWrapper<>();
        wrapper.eq("file_name",fileName);
        return selectOne(wrapper);
    }

    @Override
    public List<OssFileRecord> findAll() {
        EntityWrapper<OssFileRecord> wrapper = new EntityWrapper<>();
        ///文件访问次数升序
        wrapper.orderBy("visit_count",true);
        ////文件大小降序
        wrapper.orderBy("file_length",false);
        return selectList(wrapper);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void insertDeleteRecord(String fileName) {
        OssFileRecord ossFileRecord = new OssFileRecord();
        ossFileRecord.setVisitCount(-1);
        ossFileRecord.setFileName(fileName);
        ossFileRecord.setFileLength("-1");
        ossFileRecord.setDelFlag(true);
        ossFileRecord.setRemarks("DeleteOssFileRecord");
        insert(ossFileRecord);
    }

    @Override
    public List<OssFileRecord> findWaitDeleteRecord() {
        EntityWrapper<OssFileRecord> wrapper = new EntityWrapper<>();
        wrapper.eq("del_flag",1);
        wrapper.eq("visit_count",-1);
        return selectList(wrapper);
    }
}
