package com.unnet.yjs.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.unnet.yjs.dao.RescourceDao;
import com.unnet.yjs.entity.Rescource;
import com.unnet.yjs.service.ResourceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Email: love1208tt@foxmail.com
 * Copyright (c)  2019. missbe
 * @author lyg   19-7-1 下午9:56
 *
 *
 **/
@Service
@Transactional(readOnly = true, rollbackFor = Exception.class)
public class ResourceServiceImpl extends ServiceImpl<RescourceDao, Rescource> implements ResourceService {

    @Override
    public int getCountByHash(String hash) {
        EntityWrapper<Rescource> wrapper = new EntityWrapper<>();
        wrapper.eq("hash", hash);
        return selectCount(wrapper);
    }

    @Override
    public Rescource getRescourceByHash(String hash) {
        EntityWrapper<Rescource> wrapper = new EntityWrapper<>();
        wrapper.eq("hash", hash);
        return selectOne(wrapper);
    }
}
