/*
Navicat MySQL Data Transfer

Source Server         : 172.23.27.101:10010
Source Server Version : 50722
Source Host           : 172.23.27.101:10010
Source Database       : oneclick

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-11-25 18:04:47
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for oss_file_visit_record
-- ----------------------------
DROP TABLE IF EXISTS `oss_file_visit_record`;
CREATE TABLE "oss_file_visit_record" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  "file_name" varchar(255) DEFAULT NULL COMMENT '文件名字',
  "visit_count" int(10) DEFAULT NULL COMMENT '访问次数',
  "file_length" varchar(50) DEFAULT NULL COMMENT '文件大小',
  "create_date" datetime DEFAULT NULL COMMENT '创建时间',
  "create_by" bigint(20) DEFAULT NULL COMMENT '创建人',
  "update_date" datetime DEFAULT NULL COMMENT '修改时间',
  "update_by" bigint(20) DEFAULT NULL COMMENT '修改人',
  "remarks" varchar(255) NOT NULL COMMENT '备注:CloudDisk网盘文件|PlatformFile平台文件',
  "del_flag" tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY ("id") USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2224 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='对象存储文件访问记录';

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE "sys_log" (
  "id" bigint(50) NOT NULL AUTO_INCREMENT COMMENT '编号',
  "type" varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '请求类型',
  "title" varchar(255) COLLATE utf8_bin DEFAULT '' COMMENT '日志标题',
  "remote_addr" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作IP地址',
  "username" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '操作用户昵称',
  "request_uri" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求URI',
  "http_method" varchar(10) COLLATE utf8_bin DEFAULT NULL COMMENT '操作方式',
  "class_method" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '请求类型.方法',
  "params" text COLLATE utf8_bin COMMENT '操作提交的数据',
  "session_id" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT 'sessionId',
  "response" longtext COLLATE utf8_bin COMMENT '返回内容',
  "use_time" bigint(11) DEFAULT NULL COMMENT '方法执行时间',
  "browser" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '浏览器信息',
  "area" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '地区',
  "province" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '省',
  "city" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '市',
  "isp" varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '网络服务提供商',
  "exception" text COLLATE utf8_bin COMMENT '异常信息',
  "create_by" varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '创建者',
  "create_date" datetime DEFAULT NULL COMMENT '创建时间',
  "update_by" bigint(64) DEFAULT NULL,
  "update_date" datetime DEFAULT NULL,
  "remarks" varchar(255) COLLATE utf8_bin DEFAULT NULL,
  "del_flag" bit(1) DEFAULT NULL,
  PRIMARY KEY ("id") USING BTREE,
  KEY "sys_log_create_by" ("create_by") USING BTREE,
  KEY "sys_log_request_uri" ("request_uri") USING BTREE,
  KEY "sys_log_type" ("type") USING BTREE,
  KEY "sys_log_create_date" ("create_date") USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41132 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='系统日志';

-- ----------------------------
-- Table structure for sys_rescource
-- ----------------------------
DROP TABLE IF EXISTS `sys_rescource`;
CREATE TABLE "sys_rescource" (
  "id" bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  "area_id" varchar(25) DEFAULT NULL COMMENT '地州唯一ID',
  "file_name" varchar(255) DEFAULT NULL COMMENT '文件名称',
  "source" varchar(255) DEFAULT NULL COMMENT '来源',
  "web_url" varchar(500) DEFAULT NULL COMMENT '资源网络地址',
  "hash" varchar(255) DEFAULT NULL COMMENT '文件标识',
  "is_rename" tinyint(4) DEFAULT '1' COMMENT '文件是否重命名【0不重命名|1重命名】',
  "file_size" varchar(50) DEFAULT NULL COMMENT '文件大小',
  "file_type" varchar(255) DEFAULT NULL COMMENT '文件类型',
  "original_net_url" text,
  "create_date" datetime DEFAULT NULL COMMENT '创建时间',
  "create_by" bigint(20) DEFAULT NULL COMMENT '创建人',
  "update_date" datetime DEFAULT NULL COMMENT '修改时间',
  "update_by" bigint(20) DEFAULT NULL COMMENT '修改人',
  "remarks" varchar(255) NOT NULL COMMENT '备注:CloudDisk网盘文件|PlatformFile平台文件',
  "del_flag" tinyint(4) DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY ("id") USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2042 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统资源';
