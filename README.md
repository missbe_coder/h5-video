#### 一、功能目的
SpringBoot 实现Http分片下载断点续传，从而实现H5页面的大视频播放问题，实现渐进式播放，每次只播放需要播放的内容就可以了，不需要加载整个文件到内存中；
>ps：需要注意的几个方面:
>+ Http状态码为206，表示获取部分内容；
>+ Content-Range，格式为[要下载的开始位置]-[结束位置]/[文件总大小]；
>+ Accept-Ranges 设置为bytes；
>可以参考：
>+ [用 Java 实现断点续传 (HTTP)](https://www.ibm.com/developerworks/cn/java/joy-down/index.html) 
>+ [Content-disposition中Attachment和inline的区别](https://www.iteye.com/blog/hw1287789687-2188500) 
>+ [HTTP headers](https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/Content-Disposition)

#### 二、存储配置
>ps:文件缓存在本地，设置缓存上界，当达到了阈值时进行删除文件操作，文件删除根据访问次数，每次文件访问记录访问历史，根据访问历史来进行清除；其它配置字段如下：
```
###配置本地文件缓存位置-以/结尾【对象存储才会缓存到本地】
ok.resource.file.cache.location=/data/oneclick/video/

###配置本地视频缓存文件大小，默认5G；超过大小根据访问次数进行清理
ok.resource.file.cache.restricted.size=5
```
#### 三、部署测试
1、sql文件位置：```src/main/resources/sql/h5-video.sql```

2、测试视频位置：```src/main/resources/video/test.mp4```
在项目所在盘符的根目录下建立文件夹：```/data/oss/```.例如项目在E盘下，则视频文件目录为```E:\data\oss\test.mp4```
![位置.png](https://upload-images.jianshu.io/upload_images/11736889-d745232a64890006.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

3、启动完成后访问：[swagger-ui.html](http://localhost:9070/swagger-ui.html)
![h5-video api列表](https://upload-images.jianshu.io/upload_images/11736889-fd99ad3c13a311f3.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

4、播放页面
![测试结果](https://upload-images.jianshu.io/upload_images/11736889-4e0af9b21a2f7695.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
### 三、功能实现图
![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xMTczNjg4OS0zNDQ1ZTc4NDg5OWIzMzhhLnBuZw?x-oss-process=image/format,png)
![image.png](https://imgconvert.csdnimg.cn/aHR0cHM6Ly91cGxvYWQtaW1hZ2VzLmppYW5zaHUuaW8vdXBsb2FkX2ltYWdlcy8xMTczNjg4OS1jZWIzMDkzZTdmOWRkNDVlLnBuZw?x-oss-process=image/format,png)
#### 欢迎关注南阁公众号
![南阁子也](https://imgconvert.csdnimg.cn/aHR0cDovL2ltZy5taXNzYmUuY24vaW1hZ2VzLzIwMjAvMDMvMjYvbWlzc2JlLmpwZw?x-oss-process=image/format,png)


