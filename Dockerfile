FROM tomcat:latest
#TOMCAT环境变量
ENV CATALINA_BASE:   /usr/local/tomcat \
    CATALINA_HOME:   /usr/local/tomcat \
    CATALINA_TMPDIR: /usr/local/tomcat/temp \
    JRE_HOME:        /usr

# add create user info
MAINTAINER unnet "love1208tt@foxmail.com"
# 将webapp下的全部删除
RUN rm -rf /usr/local/tomcat/webapps/*

#启动入口
ENTRYPOINT ["catalina.sh","run"]

COPY ./target/file-upload-0.0.1-SNAPSHOT.war  ${CATALINA_HOME}/webapps/ROOT.war
EXPOSE 9080 8080 8090





